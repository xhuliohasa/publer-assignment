import {
  XCHANGE_ERROR,
  XCHANGE_REQUEST,
  XCHANGE_SUCCESS,
} from "../../Constants/actionTypes";
import { XCHANGE_RATE_API } from "../../Constants/endPoints";
import axios from "axios";

export const getXchangeRates = () => async (dispatch) => {
  await axios
    .get(XCHANGE_RATE_API)
    .then((result) => {
      const { data } = result;
      dispatch({
        type: XCHANGE_SUCCESS,
        payRates: data.rates,
        payBase: data.base,
      });
    })
    .catch((error) => {
      dispatch({ type: XCHANGE_ERROR });
    });
};
