import { XCHANGE_ERROR, XCHANGE_SUCCESS } from "../../Constants/actionTypes";

const initialState = {
  rates: {},
  base: "",
};

const XChangeReducer = (state = initialState, action) => {
  switch (action.type) {
    case XCHANGE_SUCCESS:
      return {
        ...state,
        rates: action.payRates,
        base: action.payBase,
      };
    case XCHANGE_ERROR:
      return initialState;
    default:
      return state;
  }
};

export default XChangeReducer;
