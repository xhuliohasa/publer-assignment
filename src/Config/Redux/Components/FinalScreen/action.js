import { FINAL_SCREEN_RESET } from "../../Constants/actionTypes";

// this is for when we get back to the previous screen so we can allow the app to do the calculations again
export const resetFinalScreen = () => async (dispatch) => {
  dispatch({ type: FINAL_SCREEN_RESET });
};
