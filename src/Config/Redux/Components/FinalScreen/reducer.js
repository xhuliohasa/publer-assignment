import {
  FINAL_SCREEN_SUCCESS,
  FINAL_SCREEN_RESET,
} from "../../Constants/actionTypes";

const initialState = {
  totalToShow: "",
  savedToShow: "",
  currency: "",
  success: false,
};

const FinalScreenReducer = (state = initialState, action) => {
  switch (action.type) {
    case FINAL_SCREEN_RESET:
      return initialState;
    case FINAL_SCREEN_SUCCESS:
      return {
        totalToShow: action.payTotal,
        savedToShow: action.paySaved,
        currency: action.payCurrency,
        success: true,
      };
    default:
      return state;
  }
};

export default FinalScreenReducer;
