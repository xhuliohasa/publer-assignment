import {
  FINAL_SCREEN_SUCCESS,
  INITIAL_DATA_REQUEST,
  INITIAL_DATA_SUCCESSS,
} from "../../Constants/actionTypes";
import { codeToCurrency } from "../../Constants/utils";

// This is the function that does all the Initial Screen calculations
export const totalPriceWithoutDiscounts =
  (socialAccounts, additionalMembers, isMonthly) => async (dispatch) => {
    dispatch({ type: INITIAL_DATA_REQUEST });

    // first we get how many free accounts are from social accounts
    const freeSocialAccounts = parseInt(socialAccounts / 10);

    // then we get how many have to pay from social accounts
    const paidSocialAccounts = socialAccounts - freeSocialAccounts;

    // here we get how much we save in a month from social accounts
    const priceForSocialAccountsSaved = freeSocialAccounts * 3;

    // here we get how much we pay per month from social accounts
    const priceForSocialAccountsPaidMonth = paidSocialAccounts * 3;

    // here we get how many free accounts we get from additional members
    const freeAdditionalMembers = parseInt(additionalMembers / 10);

    // here we get how many have to pay from additional members
    const paidAdditonalMambers = additionalMembers - freeAdditionalMembers;

    // here we get the saved price per month from additional members
    const priceForAdditionalMembersSaved = freeAdditionalMembers * 2;

    // here we get how much we have to pay per month from additional members
    const priceForAdditionalMembersPaidMonth = paidAdditonalMambers * 2;

    // here we get how much we pay in total per month from social accounts and additional members
    const totalWithoutDiscountPaidMonth =
      priceForSocialAccountsPaidMonth + priceForAdditionalMembersPaidMonth;

    // here we get how much we pay in total in a year from both social accounts and additional members
    const totalWithoutDiscountPaid = totalWithoutDiscountPaidMonth * 12;

    // here we get the discount depended by the period paying
    const periodDiscount =
      isMonthly === true ? 0 : totalWithoutDiscountPaid * 0.2;

    // here we get how much we save in a month from additional members and social accounts
    const savedFromAccounts =
      priceForAdditionalMembersSaved + priceForSocialAccountsSaved;

    // here we get the total we save in a year from discount and total save from accounts
    const savedAnnually = savedFromAccounts * 12 + periodDiscount;

    // here is the total we have to pay subtracting the discount from period pay
    const totalToPay = totalWithoutDiscountPaid - periodDiscount;

    dispatch(convertedPrice(totalToPay, savedAnnually));

    dispatch({
      type: INITIAL_DATA_SUCCESSS,
      additionalMembers: additionalMembers,
      socialAccounts: socialAccounts,
    });
  };

// here is the function that converts it in local currency and dispatches the values to the ResultsScreen
export const convertedPrice =
  (totalPrice, savedAnnually) => async (dispatch, state) => {
    const locationData = state().LocationReducer.locationData;
    const xChangeRates = state().XChangeReducer.rates;
    const xChangeBase = state().XChangeReducer.base;

    const countryCode = locationData.country_code;
    const currency = codeToCurrency[countryCode]
      ? codeToCurrency[countryCode]
      : xChangeBase;
    const currencyValue = xChangeRates[currency];
    const convertedToPay = (totalPrice * currencyValue).toFixed(2);
    const convertedSave = (savedAnnually * currencyValue).toFixed(2);
    dispatch({
      type: FINAL_SCREEN_SUCCESS,
      payTotal: convertedToPay,
      paySaved: convertedSave,
      payCurrency: currency,
    });
  };
