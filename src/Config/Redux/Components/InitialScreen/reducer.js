import {
  INITIAL_DATA_ERROR,
  INITIAL_DATA_REQUEST,
  INITIAL_DATA_SUCCESSS,
} from "../../Constants/actionTypes";

const initialState = {
  error: false,
  isLoading: false,
  additionalMembers: 0,
  socialAccounts: 3,
};

const InitialReducer = (state = initialState, action) => {
  switch (action.type) {
    case INITIAL_DATA_REQUEST:
      return {
        ...state,
        isLoading: true,
      };
    case INITIAL_DATA_SUCCESSS:
      return {
        ...state,
        additionalMembers: action.additionalMembers,
        socialAccounts: action.socialAccounts,
        isLoading: false,
        error: false,
      };
    case INITIAL_DATA_ERROR:
      return {
        ...state,
        isLoading: false,
        error: true,
      };
    default:
      return state;
  }
};

export default InitialReducer;
