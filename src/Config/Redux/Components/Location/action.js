import { LOCATION_API } from "../../Constants/endPoints";
import {
  LOCATION_DATA_ERROR,
  LOCATION_DATA_SUCCESS,
} from "../../Constants/actionTypes";
import axios from "axios";

export const getLocation = () => async (dispatch, state) => {
  const url = LOCATION_API;

  await axios
    .get(url)
    .then((result) => {
      const { data } = result;
      console.log("data", data);
      dispatch({ type: LOCATION_DATA_SUCCESS, payload: data });
    })
    .catch((error) => {
      dispatch({ type: LOCATION_DATA_ERROR });
    });
};
