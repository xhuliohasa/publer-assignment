import {
  LOCATION_DATA_ERROR,
  LOCATION_DATA_SUCCESS,
} from "../../Constants/actionTypes";

const initialState = {
  locationData: {
    IPv4: "",
    city: "",
    country_code: "",
    country_name: "",
    latitude: -1,
    longitude: -1,
    postal: null,
    state: "",
  },
};

const LocationReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOCATION_DATA_SUCCESS:
      return {
        locationData: action.payload,
      };
    case LOCATION_DATA_ERROR:
      return initialState;
    default:
      return state;
  }
};

export default LocationReducer;
