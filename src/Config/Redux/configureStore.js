import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import { persistStore, persistCombineReducers } from "redux-persist";
import AsyncStorage from "@react-native-async-storage/async-storage";
import InitialReducer from "./Components/InitialScreen/reducer";
import LocationReducer from "./Components/Location/reducer";
import XChangeReducer from "./Components/ChangeCurrencies/reducer";
import FinalScreenReducer from "./Components/FinalScreen/reducer";

const rehydrated = (state = false, action) => {
  switch (action.type) {
    case "persist/REHYDRATE":
      return true;
    default:
      return state;
  }
};

const reducers = {
  rehydrated,
  InitialReducer,
  LocationReducer,
  XChangeReducer,
  FinalScreenReducer,
};

const middleware = [thunk];

const config = {
  timeout: 0,
  key: "root",
  storage: AsyncStorage,
  blacklist: ["FinalScreenReducer", "InitialReducer"],
};

const allReducer = persistCombineReducers(config, reducers);

const configureStore = () => {
  const store = createStore(
    allReducer,
    applyMiddleware(...middleware),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  );

  window.store = store;

  const persistor = persistStore(store);
  return { persistor, store };
};

export default configureStore;
