import React, { useEffect, useState } from "react";
import { View, Text, StyleSheet, ActivityIndicator } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import BillingFrequency from "./components/BillingFrequency/BillingFrequency";
import LabelInputArea from "./components/LabelInput/LabelInputArea";
import { connect } from "react-redux";
import { totalPriceWithoutDiscounts } from "../../Config/Redux/Components/InitialScreen/action";
import { getLocation } from "../../Config/Redux/Components/Location/action";
import { getXchangeRates } from "../../Config/Redux/Components/ChangeCurrencies/action";

const InitialScreen = ({
  navigation,
  totalPrice,
  totalPriceWithoutDiscounts,
  getLocation,
  getXchangeRates,
  success,
  isLoading,
}) => {
  //These state hooks could be handled in a seperate file to use only functions of data managing of this app but considering
  //that there are not much we don't have to
  const [socialAccounts, setSocialAccounts] = useState(3);
  const [additionalMembers, setAdditionalMembers] = useState(0);
  const [isMonthly, setIsMonthly] = useState(true);
  const [errorMessage, setErrorMessage] = useState("");

  // These apis are to be used throughout the app so better do this call first to not cause delays later
  useEffect(() => {
    getLocation();
    getXchangeRates();
  }, []);

  useEffect(() => {
    if (success) {
      navigation.navigate("LoadingScreen");
    }
  }, [success]);

  // Handling of continue button related to the business logic
  const onContinue = () => {
    if (parseInt(socialAccounts) < 3) {
      setErrorMessage("Social accounts must be more than 3");
    } else if (socialAccounts === "" || additionalMembers === "") {
      setErrorMessage("All fields should be filled");
    } else {
      setErrorMessage("");
      totalPriceWithoutDiscounts(socialAccounts, additionalMembers, isMonthly);
    }
  };

  // All the initial components are wrapped in this return
  return (
    <View style={styles.mainContainer}>
      <View style={styles.content}>
        <View style={styles.header}>
          <Text style={styles.titleStyle}>Welcome to the Assignment</Text>
        </View>
        <View style={styles.midSection}>
          <BillingFrequency
            isMonthly={isMonthly}
            onMonthly={() => setIsMonthly(true)}
            onAnually={() => setIsMonthly(false)}
          />
          <LabelInputArea
            value={socialAccounts.toString()}
            onChange={(value) => setSocialAccounts(value)}
            title="Social Accounts"
          />

          <LabelInputArea
            value={additionalMembers.toString()}
            onChange={(value) => setAdditionalMembers(value)}
            title="Additional Members"
          />
        </View>
        {errorMessage !== "" ? (
          <View style={styles.errorContainer}>
            <Text style={styles.errorText}>{errorMessage}</Text>
          </View>
        ) : null}
        <View style={styles.footer}>
          <TouchableOpacity onPress={onContinue} style={styles.buttonContainer}>
            {isLoading ? (
              <ActivityIndicator size="small" color="white" />
            ) : (
              <Text style={styles.buttonText}>Continue</Text>
            )}
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: "white",
    flex: 1,
  },
  content: {
    alignItems: "center",
    justifyContent: "space-evenly",
    height: "100%",
  },
  header: {
    alignItems: "center",
    justifyContent: "center",
    width: "100%",
  },
  footer: {
    alignItems: "center",
    width: "100%",
  },
  midSection: {
    width: "100%",
    paddingHorizontal: 50,
  },
  titleStyle: {
    fontSize: 20,
    fontWeight: "bold",
  },
  buttonText: {
    fontWeight: "bold",
    color: "white",
  },
  buttonContainer: {
    backgroundColor: "#24B721",
    paddingVertical: 10,
    paddingHorizontal: 100,
    borderRadius: 10,
  },
  errorText: {
    color: "red",
    fontWeight: "bold",
  },
});

const mapStateToProps = ({
  InitialReducer,
  LocationReducer,
  FinalScreenReducer,
}) => {
  const { totalPrice, totalSavings, isLoading } = InitialReducer;
  const { locationData } = LocationReducer;
  const { success } = FinalScreenReducer;
  return { totalPrice, totalSavings, isLoading, locationData, success };
};

export default connect(mapStateToProps, {
  totalPriceWithoutDiscounts,
  getLocation,
  getXchangeRates,
})(InitialScreen);
