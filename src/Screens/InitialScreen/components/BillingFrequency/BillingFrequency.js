import React from "react";
import { View, Text, TouchableOpacity, StyleSheet } from "react-native";
import SwitchLabel from "./SwitchLabel";

const BillingFrequency = (props) => {
  return (
    <View style={styles.mainContainer}>
      <View style={styles.textContainer}>
        <Text style={styles.textStyle}>Billing Frequency</Text>
      </View>
      <View>
        <SwitchLabel
          isMonthly={props.isMonthly}
          onMonthly={props.onMonthly}
          onAnually={props.onAnually}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    flexDirection: "row",
    marginBottom: 30,
    justifyContent: "space-between",
  },
  textContainer: {
    marginRight: 10,
    // justifyContent: "center",
  },
  textStyle: {
    fontSize: 12,
  },
});

export default BillingFrequency;
