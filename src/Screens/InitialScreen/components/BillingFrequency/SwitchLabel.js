import React from "react";
import { View, Text, TouchableOpacity, StyleSheet } from "react-native";

const SwitchLabel = (props) => {
  const styles = StyleSheet.create({
    mainContainer: {
      flexDirection: "row",
      justifyContent: "center",
      borderWidth: 1,
      borderColor: "#24B721",
      borderRadius: 5,
      overflow: "hidden",
    },
    monthlyContainer: {
      padding: 5,
      backgroundColor: props.isMonthly ? "#24B721" : "white",
    },
    monthlyText: {
      color: props.isMonthly ? "white" : "#24B721",
    },
    anuallyContainer: {
      padding: 5,
      backgroundColor: !props.isMonthly ? "#24B721" : "white",
    },
    anuallyText: {
      color: !props.isMonthly ? "white" : "#24B721",
    },
    viewLine: {
      width: 1,
      height: "100%",
      backgroundColor: "#24B721",
    },
  });

  return (
    <View style={styles.mainContainer}>
      <TouchableOpacity
        style={styles.monthlyContainer}
        onPress={props.onMonthly}
      >
        <Text style={styles.monthlyText}>Monthly</Text>
      </TouchableOpacity>
      <View style={styles.viewLine} />
      <TouchableOpacity
        style={styles.anuallyContainer}
        onPress={props.onAnually}
      >
        <Text style={styles.anuallyText}>Anually</Text>
      </TouchableOpacity>
    </View>
  );
};

export default SwitchLabel;
