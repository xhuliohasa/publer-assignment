import React from "react";
import { View, StyleSheet, TextInput, Text } from "react-native";

const LabelInputArea = (props) => {
  return (
    <View style={styles.mainContainer}>
      <View style={styles.titleContainer}>
        <Text>{props.title}</Text>
      </View>
      <View style={styles.inputContainer}>
        <TextInput
          placeholder={props.placeholder}
          value={props.value}
          keyboardType="number-pad"
          onChangeText={props.onChange}
          style={styles.inputStyle}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: 30,
  },
  inputContainer: {
    backgroundColor: "lightgray",
    padding: 10,
    borderRadius: 5,
    minWidth: 50,
    alignItems: "center",
  },
  inputStyle: {
    textAlign: "center",
  },
  titleContainer: {
    justifyContent: "flex-end",
  },
});

export default LabelInputArea;
