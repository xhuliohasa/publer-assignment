import React, { useEffect } from "react";
import { View, StyleSheet, ActivityIndicator } from "react-native";

const LoadingScreen = ({ navigation }) => {
  // I didn't depend this on the time of Initial screen's calculations because are done too fast for they are done locally
  useEffect(() => {
    setTimeout(() => {
      navigation.navigate("ResultsScreen");
    }, 3000);
  }, []);

  return (
    <View style={styles.mainContainer}>
      <ActivityIndicator size="large" color="#24B721" />
    </View>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "white",
  },
});

export default LoadingScreen;
