import React, { useState } from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import { Appbar } from "react-native-paper";
import { connect } from "react-redux";
import { resetFinalScreen } from "../../Config/Redux/Components/FinalScreen/action";

const ResultsScreen = ({
  navigation,
  totalToShow,
  savedToShow,
  currency,
  resetFinalScreen,
}) => {
  const [buttonDisabled, setButtonDisabled] = useState(false);

  // We write this styles code in bottom because it is depended by the local state
  // Button is enabled at first so you can click it and disable it :)
  const buttonStyles = StyleSheet.create({
    buttonContainer: {
      paddingVertical: 15,
      paddingHorizontal: 100,
      borderRadius: 5,
      backgroundColor: buttonDisabled ? "lightgray" : "#24B721",
    },
    buttonText: {
      fontWeight: "bold",
      color: "white",
    },
  });

  const onBack = () => {
    resetFinalScreen();
    navigation.popToTop();
  };

  return (
    <View style={styles.mainContainer}>
      <Appbar.Header style={{ backgroundColor: "white" }}>
        <Appbar.BackAction onPress={onBack} />
      </Appbar.Header>
      <View style={styles.content}>
        <View style={styles.totalContainer}>
          <Text style={styles.totalPayTitle}>Your anual price to pay is:</Text>
          <Text
            style={styles.totalPayPrice}
          >{`${totalToShow} ${currency} / year`}</Text>

          <Text style={styles.totalPayTitle}>And you just saved:</Text>
          <Text
            style={styles.totalPayPrice}
          >{`${savedToShow} ${currency} / year`}</Text>
        </View>
        <View style={styles.footer}>
          <TouchableOpacity
            style={buttonStyles.buttonContainer}
            disabled={buttonDisabled}
            onPress={() => setButtonDisabled(!buttonDisabled)}
          >
            <Text style={buttonStyles.buttonText}>Checkout</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: "white",
    height: "100%",
  },
  content: {
    width: "100%",
    height: "80%",
    paddingTop: 100,
    alignItems: "center",
    justifyContent: "space-between",
  },
  totalPayTitle: {
    fontSize: 15,
    fontWeight: "bold",
    textAlign: "center",
  },
  totalPayPrice: {
    fontWeight: "bold",
    fontSize: 30,
    textAlign: "center",
    color: "#24B721",
    marginBottom: 50,
  },
});

const mapStateToProps = ({ FinalScreenReducer }) => {
  const { totalToShow, savedToShow, currency } = FinalScreenReducer;
  return { totalToShow, savedToShow, currency };
};

export default connect(mapStateToProps, { resetFinalScreen })(ResultsScreen);
